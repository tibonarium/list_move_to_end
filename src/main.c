#include <stdio.h>
#include <stdlib.h>

#define OE_Replace  5   /* Delete existing record, then do INSERT or UPDATE */
#define UINT8_TYPE unsigned char

typedef UINT8_TYPE u8;             /* 1-byte unsigned integer */
typedef struct Table Table;
typedef struct Index Index;

struct Index {
  Index *pNext;            /* The next index associated with the same table */
  u8 onError;              /* OE_Abort, OE_Ignore, OE_Replace, or OE_None */
};

struct Table {
  Index *pIndex;       /* List of SQL indexes on this table. */
};

void correct_move_indexes_to_end(Table* pTab) {
  if( pTab ){  /* Ensure all REPLACE indexes are at the end of the list */
    Index **ppFrom = &pTab->pIndex;
    Index *pThis;
    for(ppFrom=&pTab->pIndex; (pThis = *ppFrom)!=0; ppFrom=&pThis->pNext){
      Index *pNext;
      Index *pTail = pThis;
      if( pThis->onError!=OE_Replace ) continue;
      while( (pNext = pThis->pNext)!=0 ){
        if (pNext->onError!=OE_Replace) {
          *ppFrom = pNext;
          pThis->pNext = pNext->pNext;
          pNext->pNext = pTail;
          ppFrom = &pNext->pNext;
        } else {
          pThis = pNext;
        }
      }
      break;
    }
  }
}

void move_indexes_to_end(Table* pTab) {
  if( pTab ){  /* Ensure all REPLACE indexes are at the end of the list */
    Index **ppFrom = &pTab->pIndex;
    Index *pThis;
    for(ppFrom=&pTab->pIndex; (pThis = *ppFrom)!=0; ppFrom=&pThis->pNext){
      Index *pNext;
      if( pThis->onError!=OE_Replace ) continue;
      while( (pNext = pThis->pNext)!=0 && pNext->onError!=OE_Replace ){
        *ppFrom = pNext;
        pThis->pNext = pNext->pNext;
        pNext->pNext = pThis;
        ppFrom = &pNext->pNext;
      }
      break;
    }
  }
}

void print_indexes(Table* pTab) {
  if( pTab ){ 
    Index *pIndex;
    int i = 0;
    for(pIndex = pTab->pIndex; pIndex; pIndex=pIndex->pNext, i++){
      printf (
        "index: %d onError: %s pIndex: %p pNext: %p\n", 
        i, 
        pIndex->onError == OE_Replace ? "OE_Replace" : "OE_Other", 
        (void*)pIndex, 
        (void*)pIndex->pNext);
    }
  }
}

void fill_indexes(Table* pTab) {
  if( pTab ){ 
    Index *pIndex;
    int i;

    for(i=0; i<=6; i++) {
      pIndex = malloc(sizeof(Index)); 
      if (i % 2 != 0) {
        pIndex->onError = OE_Replace;
      }
      pIndex->pNext = pTab->pIndex;
      pTab->pIndex = pIndex;
    }
  }
}

void clear_indexes(Table* pTab) {
  if( pTab ){ 
    Index *pIndex;
    Index* pThis;
    for(pIndex = pTab->pIndex; pIndex;){
      pTab->pIndex = pIndex->pNext;
      pThis = pIndex;
      pIndex = pIndex->pNext;
      free(pThis);
    }
  }
}

int main()
{
  Table *pTab = 0;     /* Table to be indexed */

  pTab = malloc(sizeof(Table)); 
  fill_indexes(pTab);

  printf("Before moving...\n");
  print_indexes(pTab);

  move_indexes_to_end(pTab);

  printf("After moving...\n");
  print_indexes(pTab);

  clear_indexes(pTab);
  fill_indexes(pTab);

  correct_move_indexes_to_end(pTab);

  printf("After correct moving...\n");
  print_indexes(pTab);

  clear_indexes(pTab);
  free(pTab);

	return (0);
}