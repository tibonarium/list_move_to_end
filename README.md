# Example project

While reading sqlite source code I encountered code to move elements with specific property
to the end of the linked list. I had a suspicion that there was an error in the implementation.
And created this project to demonstrate specific problem.

In the file [build.c](https://github.com/sqlite/sqlite/blob/master/src/build.c) inside function `sqlite3CreateIndex` there is a comment `Ensure all REPLACE indexes are at the end of the list`. But this implementation will not move all elements with property `onError=OE_Replace` to the end of the list.
```c
if( pTab ){  /* Ensure all REPLACE indexes are at the end of the list */
  Index **ppFrom = &pTab->pIndex;
  Index *pThis;
  for(ppFrom=&pTab->pIndex; (pThis = *ppFrom)!=0; ppFrom=&pThis->pNext){
    Index *pNext;
    if( pThis->onError!=OE_Replace ) continue;
    while( (pNext = pThis->pNext)!=0 && pNext->onError!=OE_Replace ){
      *ppFrom = pNext;
      pThis->pNext = pNext->pNext;
      pNext->pNext = pThis;
      ppFrom = &pNext->pNext;
    }
    break;
  }
}
```

We can fix it this way
```c
if( pTab ){  /* Ensure all REPLACE indexes are at the end of the list */
  Index **ppFrom = &pTab->pIndex;
  Index *pThis;
  for(ppFrom=&pTab->pIndex; (pThis = *ppFrom)!=0; ppFrom=&pThis->pNext){
    Index *pNext;
    Index *pTail = pThis;
    if( pThis->onError!=OE_Replace ) continue;
    while( (pNext = pThis->pNext)!=0 ){
      if (pNext->onError!=OE_Replace) {
        *ppFrom = pNext;
        pThis->pNext = pNext->pNext;
        pNext->pNext = pTail;
        ppFrom = &pNext->pNext;
      } else {
        pThis = pNext;
      }
    }
    break;
  }
}
```

